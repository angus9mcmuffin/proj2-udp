import sys
import getopt

import Checksum
import BasicSender

'''
This is a skeleton sender class. Create a fantastic transport protocol here.
'''
class Sender(BasicSender.BasicSender):
    def __init__(self, dest, port, filename, debug=False, sackMode=False):
        super(Sender, self).__init__(dest, port, filename, debug)
        self.sackMode = sackMode
        self.debug = debug

    # Main sending loop.
    def start(self):
        # add things here
    #Make some data packets
        packets = []
        payload = self.infile.read(1400)
        i = 0
        while payload:
            i = i + 1
            print("Trying to make the packet " + str(i))
            packet = self.make_packet("dat", i, payload)
            packets.append(packet)
            payload = self.infile.read(1400)
        fin_packet = packets[-1]
        a, fin_seqno, fin_data, d = self.split_packet(fin_packet)
        packets[-1] = self.make_packet("fin", int(fin_seqno), fin_data)
    # Packets are all ready for sending!
        #This sends out a syn packet to establish handshake
        msg = self.make_packet("syn", 0, None)
        self.send(msg)
        ack1 = self.receive(0.5)
        while ack1 is None or not Checksum.validate_checksum(ack1):
            self.send(msg)
            ack1 = self.receive(0.5)
        # Now the BCP
        if self.sackMode:
            #do something
            self.sack_send(packets)
        else:
            #do something else
            self.cumack_send(packets)


    def cumack_send(self, packets):
        packets_ptr = 1
        for packet in packets[packets_ptr-1:packets_ptr + 6]:
            self.send(packet)    
        ack_count = 0
        while packets_ptr < len(packets):
            ack = self.receive(.5)
            if ack is None:
                for packet in packets[packets_ptr-1:packets_ptr + 6]:
                    self.send(packet)
            elif not Checksum.validate_checksum(ack):
                pass
            else:
                a, seqno, c, d = self.split_packet(ack)
                seqno = int(seqno)
                if packets_ptr == seqno:
                    ack_count += 1
                    if ack_count == 3:
                        ack_count = 0
                        self.send(packets[packets_ptr - 1])
                elif packets_ptr < seqno:
                    for packet in packets[packets_ptr + 6:seqno + 6]:
                        self.send(packet)
                    packets_ptr = seqno 
                    ack_count = 0


    def sack_send(self, packets):
        packets_ptr = 1
        packets_to_send = []
        for i in range(len(packets)):
            packets_to_send.append(i + 1)
            if i < 7:
                self.send(packets[i])    
        ack_count = 0

        while len(packets_to_send) > 0:
            ack = self.receive(.5)
            if ack is None:
                for i in range(7):
                    if packets_ptr + i in packets_to_send:
                        self.send(packets[packets_ptr + i])
            elif not Checksum.validate_checksum(ack):
                pass
            else:
                a, seqno, c, d = self.split_packet(ack)
                acks = seqno.split(';')
                cumack = int(acks[0])
                has_sacks = False
                if len(acks) > 1:
                    sacks = acks[1].split(',')
                    has_sacks = True
                if packets_ptr == cumack:
                    ack_count += 1
                    if ack_count == 3:
                        ack_count = 0
                        self.send(packets[packets_ptr - 1])
                    if has_sacks:
                        for sack_val in sacks:
                            sack_val = int(sack_val)
                            try:
                                packets_to_send.remove(sack_val)
                            except:
                                pass
                elif packets_ptr < cumack:
                    if has_sacks:
                        for sack_val in sacks:
                            sack_val = int(sack_val)
                            try:
                                packets_to_send.remove(sack_val)
                            except:
                                pass
                    for val in range(cumack - packets_ptr):
                        try:
                            packets_to_send.remove(packets_ptr + val)
                        except:
                            pass
                    for packet in packets[packets_ptr + 6:cumack + 6]:
                        self.send(packet)
                    packets_ptr = cumack 
                    ack_count = 0



        
'''
This will be run if you run this script from the command line. You should not
change any of this; the grader may rely on the behavior here to test your
submission.
'''
if __name__ == "__main__":
    def usage():
        print "BEARS-TP Sender"
        print "-f FILE | --file=FILE The file to transfer; if empty reads from STDIN"
        print "-p PORT | --port=PORT The destination port, defaults to 33122"
        print "-a ADDRESS | --address=ADDRESS The receiver address or hostname, defaults to localhost"
        print "-d | --debug Print debug messages"
        print "-h | --help Print this usage message"
        print "-k | --sack Enable selective acknowledgement mode"

    try:
        opts, args = getopt.getopt(sys.argv[1:],
                               "f:p:a:dk", ["file=", "port=", "address=", "debug=", "sack="])
    except:
        usage()
        exit()

    port = 33122
    dest = "localhost"
    filename = None
    debug = False
    sackMode = False

    for o,a in opts:
        if o in ("-f", "--file="):
            filename = a
        elif o in ("-p", "--port="):
            port = int(a)
        elif o in ("-a", "--address="):
            dest = a
        elif o in ("-d", "--debug="):
            debug = True
        elif o in ("-k", "--sack="):
            sackMode = True

    s = Sender(dest,port,filename,debug, sackMode)
    try:
        s.start()
    except (KeyboardInterrupt, SystemExit):
        exit()
